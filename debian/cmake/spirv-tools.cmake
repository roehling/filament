if(NOT TARGET SPIRV-Tools-opt)
    find_package(PkgConfig REQUIRED)
    pkg_check_modules(SPIRV-Tools REQUIRED IMPORTED_TARGET SPIRV-Tools)
    add_library(SPIRV-Tools-opt INTERFACE)
    target_link_libraries(SPIRV-Tools-opt INTERFACE PkgConfig::SPIRV-Tools)
endif()

