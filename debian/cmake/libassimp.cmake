# find_package(assimp) and target assimp::assimp is broken, so we do this manually
if(NOT TARGET assimp)
    find_path(assimp_INCLUDE_DIR NAMES assimp/version.h)
    find_library(assimp_LIBRARY NAMES assimp)
    add_library(assimp INTERFACE)
    target_include_directories(assimp INTERFACE ${assimp_INCLUDE_DIR})
    target_link_libraries(assimp INTERFACE ${assimp_LIBRARY})
endif()

