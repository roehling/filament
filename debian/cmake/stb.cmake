if(NOT TARGET filament::stb)
    find_path(stb_INCLUDE_DIR NAMES stb.h PATH_SUFFIXES stb)
    find_library(stb_LIBRARY NAMES stb)
    if(stb_INCLUDE_DIR AND stb_LIBRARY)
        add_library(filament::stb INTERFACE IMPORTED)
        set_target_properties(filament::stb PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${stb_INCLUDE_DIR}"
            INTERFACE_LINK_LIBRARIES "${stb_LIBRARY}"
        )
    endif()
endif()
