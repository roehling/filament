find_package(benchmark REQUIRED)
if(NOT TARGET benchmark)
    add_library(benchmark INTERFACE)
    target_link_libraries(benchmark INTERFACE benchmark::benchmark)
endif()
if(NOT TARGET benchmark_main)
    add_library(benchmark_main INTERFACE)
    target_link_libraries(benchmark_main INTERFACE benchmark::benchmark_main)
endif()
