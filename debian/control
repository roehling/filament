Source: filament
Section: libs
Priority: optional
Maintainer: Timo Röhling <roehling@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 clang:native,
 cmake,
 libfilament-tools <cross>,
 libassimp-dev,
 libastcenc-dev,
 libbenchmark-dev,
 libdraco-dev,
 libepoxy-dev,
 libgtest-dev,
 libimgui-dev,
 libjsmn-dev,
 libmeshoptimizer-dev,
 libpng-dev,
 libsdl2-dev,
 libstb-dev,
 libtinyexr-dev,
 robin-map-dev,
 zlib1g-dev,
Homepage: https://github.com/google/filament
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/roehling/filament.git
Vcs-Browser: https://salsa.debian.org/roehling/filament

Package: libfilament1.9
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Real-time physically based rendering engine
 Physically based rendering is a rendering method that provides a more
 accurate representation of materials and how they interact with light
 when compared to traditional real-time models.

Package: libfilament-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 libfilament1.9 (= ${binary:Version}),
 libimgui-dev,
 libstb-dev,
 robin-map-dev,
Recommends: libfilament-tools (= ${binary:Version})
Description: Real-time physically based rendering engine - development headers
 Physically based rendering is a rendering method that provides a more
 accurate representation of materials and how they interact with light
 when compared to traditional real-time models.
 .
 This package installs the development headers.

Package: libfilament-tools
Section: devel
Architecture: any
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Real-time physically based rendering engine - command-line tools
 Physically based rendering is a rendering method that provides a more
 accurate representation of materials and how they interact with light
 when compared to traditional real-time models.
 .
 This package installs the command-line tools for material preprocessing.
